<?php
namespace App\Controller;

use App\Controller\AppController;
/**
 * Tags Controller
 *
 * @property \App\Model\Table\TagsTable $Tags
 */
class TagsController extends AppController
{
		var $uses = array('Bookmarker');
		
		public function isAuthorized($user)
		{
				$action = $this->request->params['action'];

				// The add and index actions are always allowed.
				if (in_array($action, ['index', 'add', 'tags'])) {
						return true;
				}
				// All other actions require an id.
				if (empty($this->request->params['pass'][0])) {
						return false;
				}

				// Check that the bookmark belongs to the current user.
				$id = $this->request->params['pass'][0];
				$tag = $this->Tags->get($id);
				//if ($tag->user_id == $user['id']) {
						return true;
				//}
				//return parent::isAuthorized($user);
		}
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
				$this->set('tags', $this->paginate($this->Tags));
    }

    /**
     * View method
     *
     * @param string|null $id Tag id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tag = $this->Tags->get($id, [
            'contain' => ['Bookmarks']
        ]);
        $this->set('tag', $tag);
        $this->set('_serialize', ['tag']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tag = $this->Tags->newEntity();
        if ($this->request->is('post')) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
						$tag->user_id = $this->Auth->user('id');
            if ($this->Tags->save($tag)) {
                $this->Flash->success('The tag has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The tag could not be saved. Please, try again.');
            }
        }
				$this->loadModel('Bookmarks');
				$bookmarks = $this->Bookmarks->find('list');
				$this->set(compact('bookmarks', 'tag'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tag id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tag = $this->Tags->get($id, [
            'contain' => ['Bookmarks']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tag = $this->Tags->patchEntity($tag, $this->request->data);
            $bookmark->user_id = $this->Auth->user('id');
						if ($this->Tags->save($tag)) {
                $this->Flash->success('The tag has been saved.');
                return $this->redirect(['action' => 'index']);
            }
						$this->Flash->error('The tag could not be saved. Please, try again.');
        }
				//$this->loadModel('Bookmarks');
				//$tag = $this->Tags->get($id);
				$bookmarks = $this->Tags->Bookmarks->find('list');
				$this->set(compact('bookmarks', 'tag'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tag id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tag = $this->Tags->get($id);
        if ($this->Tags->delete($tag)) {
            $this->Flash->success('The tag has been deleted.');
        } else {
            $this->Flash->error('The tag could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
		
//		public function bookmarks()
//    {
//        $tags = $this->request->params['pass'];
//        $bookmarks = $this->Tags->find('tagged', [
//            'tags' => $tags
//        ]);
//        $this->set(compact('bookmarks', 'tags'));
//    }
}
