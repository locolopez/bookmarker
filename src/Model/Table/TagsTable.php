<?php
namespace App\Model\Table;

use App\Model\Entity\Tag;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tags Model
 */
class TagsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('tags');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsToMany('Bookmarks', [
            'foreignKey' => 'tag_id',
            'targetForeignKey' => 'bookmark_id',
            'joinTable' => 'bookmarks_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->allowEmpty('title');

        return $validator;
    }
		
//		public function findTagged(Query $query, array $options)
//    {
//        $fields = [
//            'Bookmarks.id',
//            'Bookmarks.title',
//            'Bookmarks.url',
//        ];
//        return $this->find()
//            ->distinct($fields)
//            ->matching('Tags', function ($q) use ($options) {
//                return $q->where(['Tags.title IN' => $options['tags']]);
//            });
//    }
}
